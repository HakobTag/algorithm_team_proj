import React from "react";
import * as S from "./styles";
import { ColumnCard } from "../ColumnCard/ColumnCard";

export const SingleColumn = ({ label }) => {
  return (
    <S.Wrapper>
      <S.TitleWrapper>
        <S.TitleLabel>{label}</S.TitleLabel>
      </S.TitleWrapper>
      <S.Content>
        <ColumnCard label={label} />
      </S.Content>
    </S.Wrapper>
  );
};
