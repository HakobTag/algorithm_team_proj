import styled from "styled-components"

export const Wrapper = styled.div`
    width: 20%;
    height: 100%;
    display: flex;
    flex-direction: column;
    background-color: #f8f2fd;
`
export const TitleWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  /* box-shadow: 0px 3px 15px 0 rgba(8, 4, 158, 0.3); */
  border-bottom: solid 1px #0b1530;
`
export const TitleLabel = styled.h2`
  color: #162651;
  font-size: 1em;
`
export const Content = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    padding: 10px;
    border-right: solid 1px #f0e6f8;
    box-shadow: 0px -3px 1px 0 rgba(8, 4, 158, 0.3);
`