import React from "react";
import * as S from "./styles";
import { SingleColumn } from "../SingleColumn/SingleColumn";

const columnList = ["TO DO", "IN PROGRESS", "REVIEW", "TEST", "PRODUCTION"];

export const Columns = () => {
  return (
    <S.Wrapper>
      {columnList.map((col) => (
        <SingleColumn label={col} />
      ))}
    </S.Wrapper>
  );
};
