import styled from "styled-components";

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    border-radius: 3px;
    padding: 5px;
    background-color: #eee;
`
