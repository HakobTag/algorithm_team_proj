import React from "react";
import * as S from "./styles";

export const ColumnCard = ({ label }) => {
  return <S.Wrapper>{label}</S.Wrapper>;
};
